package tui;

import java.util.Scanner;

import business.PessoaBO;

import domain.Professor;
import domain.enums.TitulacaoEnum;
import exception.FalhaException;

public class ProfessorTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void cadastrarProfessorComTelefone() throws FalhaException {
		String nome;
		String nacionalidade;
		String telefone;
		String escolha;
	
		System.out.println("Digite o seu nome: ");
		nome = sc.next();
		sc.nextLine();
		System.out.println("Digite a sua nacionalidade:  ");
		nacionalidade = sc.next();
		sc.nextLine();
		System.out.println("Digite o seu telefone:  ");
		telefone = sc.next();
		sc.nextLine();
		System.out.println("Digite a sua Titulacao\n" +"MESTRE  |  ESPECIALISTA  |  DOUTOR ");
		escolha = sc.next();

	
		
		Professor professor = new Professor(nome, nacionalidade, telefone, TitulacaoEnum.valueOf(escolha));
			
		PessoaBO.cadastrarProfessor(professor);
		System.out.println("Professor cadastrado com sucesso.");
		}

	public static void cadastrarProfessorSemTelefone() throws FalhaException {
		String nome;
		String nacionalidade;
		
		String escolha;
	
		System.out.println("Digite o seu nome: ");
		nome = sc.next();
		sc.nextLine();
		System.out.println("Digite a sua nacionalidade:  ");
		nacionalidade = sc.next();
		sc.nextLine();
		System.out.println("Digite a sua Titulacao\n" +"MESTRE  |  ESPECIALISTA  |  DOUTOR ");
		escolha = sc.next();

	
		
		Professor professor = new Professor(nome, nacionalidade, TitulacaoEnum.valueOf(escolha));
			
		PessoaBO.cadastrarProfessor(professor);
		System.out.println("Professor cadastrado com sucesso.");
		}
	}

