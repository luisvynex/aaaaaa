package tui;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

import business.PessoaBO;
import domain.Aluno;
import domain.Pessoa;
import domain.Professor;
import exception.FalhaException;
import persistence.PessoaDAO;

public class PessoaTUI {

	private static Scanner sc = new Scanner(System.in);

	private static Professor professor;
	private static int escolha;
	private static int escolha2;

	private static List<Pessoa> pessoas;


	public static void main(String[] args) throws FalhaException {
		executarMenu();
	}

	private static void executarMenu() throws FalhaException {
		do {
			executarFuncoes();
		} while (escolha != 9);
	}

	private static void executarFuncoes() throws FalhaException {
		System.out.println("Escolha uma função do nosso sistema: ");
		System.out.println(
				"(1) CADASTRO PROFESSOR\n" + "(2) CADASTRO ALUNO\n" + "(3) LISTAR ORIGEM ESCOLAR " + "(9) SAIR");
		escolha = sc.nextInt();

		switch (escolha) {

		case 1:
			System.out.println("Deseja cadastrar seu telefone?\n" + "(1) SIM 	(2) NÃO\n");
			escolha2 = sc.nextInt();
			if (escolha2 == 1) {
				ProfessorTUI.cadastrarProfessorComTelefone();
			} else {
				ProfessorTUI.cadastrarProfessorSemTelefone();
			}
			break;
		case 2:
			AlunoTUI.cadastrarAlunoTUI();
			break;
		case 3:
			PessoaDAO.retornarPessoa();
		case 9:
			System.out.println("\n******Sistema encerrado!******\n" + "	   Até logo");

		default:
			break;
		}

	}

}
