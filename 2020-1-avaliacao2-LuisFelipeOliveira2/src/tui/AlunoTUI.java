package tui;

import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import business.PessoaBO;
import domain.Aluno;
import domain.Pessoa;
import exception.FalhaException;

public class AlunoTUI {
	private static Scanner sc = new Scanner(System.in);

	public static void cadastrarAlunoTUI() throws FalhaException {
		String nome;
		String nacionalidade;
		String telefone;
		String escolaOrigem;
		Integer anoNascimento;

		System.out.println("Digite o seu nome: ");
		nome = sc.next();
		sc.nextLine();
		System.out.println("Digite a sua nacionalidade:  ");
		nacionalidade = sc.next();
		sc.nextLine();
		System.out.println("Digite o seu telefone:  ");
		telefone = sc.next();
		sc.nextLine();
		System.out.println("Digite a sua escola de origem:  ");
		escolaOrigem = sc.next();
		sc.nextLine();
		System.out.println("Digite o seu ano de nascimento:  ");
		anoNascimento = sc.nextInt();
		sc.nextLine();

		Aluno aluno = new Aluno(nome, telefone, nacionalidade, anoNascimento, escolaOrigem);
		System.out.println(aluno);
	
			PessoaBO.cadastrarAluno(aluno);
	}

	public static void exibirNomes(List<Pessoa> pessoas) throws FalhaException {
		
		System.out.println("Alunos Informados: ");
		for (Pessoa aluno : pessoas) {
			System.out.println(aluno);
		}
		
		PessoaBO.obterNomesEscolaOrigem();
		

	}

}
