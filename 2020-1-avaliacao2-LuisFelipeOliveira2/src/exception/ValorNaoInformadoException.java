package exception;

public class ValorNaoInformadoException extends Exception {


	private static final long serialVersionUID = -3163507537957450623L;

	public ValorNaoInformadoException(String mensagem) {
		super(mensagem);
	}
}
