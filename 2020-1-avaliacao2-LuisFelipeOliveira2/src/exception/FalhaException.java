package exception;

public class FalhaException extends Exception {

	private static final long serialVersionUID = -3899481405158698886L;

	public FalhaException(String mensagem) {
		super(mensagem);
	}
}
