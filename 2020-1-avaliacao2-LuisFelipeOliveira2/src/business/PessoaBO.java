package business;

import java.util.List;
import java.util.Set;

import domain.Aluno;
import domain.Professor;
import exception.FalhaException;
import persistence.PessoaDAO;
import domain.Pessoa;

public class PessoaBO {

	public static void cadastrarProfessor(Professor professor) throws FalhaException {

		try {
			PessoaDAO.cadastrar(professor);
			System.out.println("Não foi possível cadastrar o professor");
			throw new FalhaException("");
		} catch (FalhaException mensage) {
			System.out.println("Professor cadastrado com sucesso.");
		}

	}

	public static void cadastrarAluno(Aluno aluno) throws FalhaException {

		try {
			PessoaDAO.cadastrar(aluno);
			throw new FalhaException("Não foi possível cadastrar o aluno");
		} catch (FalhaException mensage) {
			System.out.println("Aluno cadastrado com sucesso");
		}

	}

	public static List<Pessoa> obterNomesEscolaOrigem() throws FalhaException {
		return PessoaDAO.retornarPessoa();
	
	}

	public static void obterPessoasOrdemNome() {

	}

	public static String obterPessoasOrdemNacionalidadeNome() {
		return null;

	}

}
