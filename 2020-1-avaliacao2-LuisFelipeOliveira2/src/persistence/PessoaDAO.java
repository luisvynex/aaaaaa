package persistence;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import domain.Aluno;
import domain.Pessoa;
import domain.Professor;

public class PessoaDAO {

	public static List<Pessoa> pessoas = new ArrayList<Pessoa>();
	public static List<Aluno> alunos = new ArrayList<>();

	public static void cadastrar(Pessoa pessoa) {
		pessoas.add(pessoa);

	}

	public static List<Pessoa> retornarPessoa() {

		return pessoas;

	}

}
