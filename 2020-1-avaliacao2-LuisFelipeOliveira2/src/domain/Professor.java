package domain;

import domain.enums.TitulacaoEnum;

public class Professor extends Pessoa {

	private String telefone;
	private TitulacaoEnum titulacao;

	public Professor(String nome, String nacionalidade, TitulacaoEnum titulacao) {
		super(nome, nacionalidade);
		this.titulacao = titulacao;

	}

	public Professor(String nome, String nacionalidade, String telefone, TitulacaoEnum titulacao) {
		super(nome, nacionalidade);
		this.telefone = telefone;
		this.titulacao = titulacao;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public TitulacaoEnum getTitulacao() {
		return titulacao;
	}

	public void setTitulacao(TitulacaoEnum titulacao) {
		this.titulacao = titulacao;
	}

	@Override
	public String toString() {
		if (telefone != null) {
			return "Professor: " + super.toString() + telefone + telefone + " Titulacao: " + titulacao;

		}
		return "Professor: " + super.toString() + " Titulacao: " + titulacao;
	}
}
