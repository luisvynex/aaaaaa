package domain;

public class Pessoa {

	private String nacionalidade;
	private String nome;

	public String getNacionalidade() {
		return nacionalidade;
	}

	public Pessoa(String nome, String nacionalidade) {
		super();
		this.nacionalidade = nacionalidade;
		this.nome = nome;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Nome: " + nome + "Nacionalidade: " + nacionalidade;
	}

}
