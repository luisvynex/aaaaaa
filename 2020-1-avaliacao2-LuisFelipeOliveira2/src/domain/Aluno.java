package domain;

import java.util.Comparator;

public class Aluno extends Pessoa implements Comparable<Aluno> {

	private String telefone;
	private String escolaOrigem;
	private Integer anoNascimento;

	public Aluno(String nome, String telefone, String nacionalidade, Integer anoNascimento, String escolaOrigem) {
		super(nacionalidade, nome);
		this.telefone = telefone;
		this.anoNascimento = anoNascimento;
		this.escolaOrigem = escolaOrigem;

	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEscolaOrigem() {
		return escolaOrigem;
	}

	public void setEscolaOrigem(String escolaOrigem) {
		this.escolaOrigem = escolaOrigem;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	@Override
	public String toString() {
		return "Aluno:\n " + super.toString() + " telefone:	" + telefone + "Escola de origem:	" + escolaOrigem
				+ "	Ano de Nascimento:	" + anoNascimento;
	}

	@Override
	public int compareTo(Aluno outroObjeto) {
		
		return this.getNome().compareTo(outroObjeto.getNome());
	}
	
	
		
	}

